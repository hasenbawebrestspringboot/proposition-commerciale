
package application;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.web.EmbeddedServletContainerAutoConfiguration;
import org.springframework.boot.autoconfigure.web.ServerPropertiesAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

/**
 * Configuration de l'application.
 *
 * @author Hasen Baweb
 */
@Configuration
@ComponentScan
@ImportResource(value = "classpath:spring/applicationContext.xml")
@Import({ EmbeddedServletContainerAutoConfiguration.class, ServerPropertiesAutoConfiguration.class })
public class Application {

    @Value("${wrf.jvm.timezone:UTC}")
    private String timeZone;

    @PostConstruct
    void started() {
        TimeZone.setDefault(TimeZone.getTimeZone(timeZone));
    }

    /**
     * Méthode principale de lancement de l'application.
     *
     * @param args
     *            Les arguments de lancement.
     */
    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
