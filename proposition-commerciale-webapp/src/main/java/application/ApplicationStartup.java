
package application;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * Listener sur le démarrage de l'application.
 *
 * @author Hasen Baweb
 *
 */
@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {

        // gerer le cache
        // => possibilité de creer un service de gestion du cache.
    }

}
